import application.gui.GameView;
import application.logic.APIFactory;
import application.logic.APIFactoryImpl;
import application.logic.api.GameModel;
import application.logic.api.impl.GameModelImpl;

public class Main {

    public static void main(String[] args) {
        GameView gui = new GameView();
        gui.startEventLoop();
    }
}
