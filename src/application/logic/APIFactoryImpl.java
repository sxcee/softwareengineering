package application.logic;

import application.logic.api.GameModel;
import application.logic.api.impl.GameModelImpl;

/**
 * Created by danielmauch on 13.06.16.
 */
public class APIFactoryImpl implements APIFactory{

    private GameModel model;
    private static APIFactoryImpl theFactory;

    private APIFactoryImpl(){}

    public GameModel getModel() {
        return this.getModelImpl();
    }

    private GameModel getModelImpl() {
        if(this.model == null) {
            this.model = new GameModelImpl();
        }
        return this.model;
    }

    static APIFactory makeFactory() {
        if (APIFactoryImpl.theFactory == null) {
            APIFactoryImpl.theFactory = new APIFactoryImpl();
        }
        return APIFactoryImpl.theFactory;
    }
}
