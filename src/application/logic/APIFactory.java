package application.logic;

import application.logic.api.GameModel;

/**
 * Created by danielmauch on 13.06.16.
 */
public interface APIFactory {

    APIFactory factory = APIFactoryImpl.makeFactory();

    GameModel getModel();
}
