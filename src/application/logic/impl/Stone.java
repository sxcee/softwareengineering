package application.logic.impl;

import java.awt.*;

/**
 * Created by danielmauch on 13.06.16.
 */
public class Stone {

    private PlayerColor color;
    private int stoneNr;
    private int field;

    public Stone(PlayerColor color, int stoneNr) {
        this.color = color;
        this.stoneNr = stoneNr;
        this.field = -1;
    }

    public PlayerColor getColor() {
        return color;
    }

    public void setColor(PlayerColor color) {
        this.color = color;
    }

    public int getStoneNr() {
        return stoneNr;
    }

    public void setStoneNr(int stoneNr) {
        this.stoneNr = stoneNr;
    }

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field % 48;
    }
}
