package application.logic.impl;

/**
 * Created by danielmauch on 13.06.16.
 */
public enum State {
    SETUP("setup"),
    INIT("init"),
    DARFWUERFELN("darfwuerfeln"),
    DARFZIEHEN("darfziehen"),
    DARFRAUS("darfraus"),
    WUERFELERGEBNIS_EVAL("wuerfelergebnis_eval"),
    ZIEHEN_EVAL("ziehen_eval"),
    ENDE("zugende");

    State(String value){
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
