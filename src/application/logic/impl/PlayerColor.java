package application.logic.impl;

/**
 * Created by danielmauch on 13.06.16.
 */
public enum PlayerColor {

    RED(0),
    BLUE(1),
    YELLOW(2),
    GREEN(3);

    private int value;

    PlayerColor(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public PlayerColor getColorFromNumber(int number) {
        if(number == 1) {
            return RED;
        }
        if(number == 2) {
            return BLUE;
        }
        if(number == 3) {
            return YELLOW;
        }
        if(number == 4) {
            return GREEN;
        }

        return null;
    }

    public String toString(){
        if (this.getValue() == 0) return "Roter ";
        if (this.getValue() == 1) return "Blauer ";
        if (this.getValue() == 2) return "Gelber ";
        if (this.getValue() == 3) return "Grüner ";

        return "";
    }
}
