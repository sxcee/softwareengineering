package application.logic.impl;

/**
 * Created by danielmauch on 14.06.16.
 */
public enum OUTPUT {

    BASE_OCCUPIED("base nicht frei"),
    NO_SIX("keine sechs gewürfelt"),
    NO_MESSAGE("kein messgage ausgeben"),
    FIELD_OCCUPIED("feld belegt"), //Fragerunde
    SUCCESS_NEWSTONE("neuer stein"),
    SUCCES_MOVESTONE("stein verschieben"),
    FIELD_OCCUPIED_OWN("eigener stein");

    OUTPUT(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }
}
