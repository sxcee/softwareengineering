package application.logic.impl;

import java.util.ArrayList;

/**
 * Created by danielmauch on 13.06.16.
 */
public class Players {

    private ArrayList<Player> players;
    private int amountPlayers;

    public Players(){
        players = new ArrayList<>();
        amountPlayers = 0;
    }

    public boolean addPlayer(Player player) {
        if (player != null) {
            players.add(player);
            return true;
        }
        return false;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
}
