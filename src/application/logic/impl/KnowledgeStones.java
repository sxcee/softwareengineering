package application.logic.impl;

import java.util.ArrayList;

/**
 * Created by danielmauch on 13.06.16.
 */
public class KnowledgeStones {

    private ArrayList<KnowledgeStone> knowledgeStones;

    public KnowledgeStones(){
        knowledgeStones = new ArrayList<>();

        KnowledgeStone firstStone = new KnowledgeStone(Categories.CATEGORY_ONE);
        KnowledgeStone secondStone = new KnowledgeStone(Categories.CATEGORY_TWO);
        KnowledgeStone thirdStone = new KnowledgeStone(Categories.CATEGORY_THREE);
        KnowledgeStone fourthStone = new KnowledgeStone(Categories.CATEGORY_FOUR);
        knowledgeStones.add(firstStone);
        knowledgeStones.add(secondStone);
        knowledgeStones.add(thirdStone);
        knowledgeStones.add(fourthStone);
    }


}
