package application.logic.impl;

/**
 * Created by danielmauch on 13.06.16.
 */
public class KnowledgeStone {

    private int score;
    private Categories category;

    public KnowledgeStone(Categories category){
        this.category = category;
        this.score = 0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Categories getCategory() {
        return category;
    }
}
