package application.logic.impl;

/**
 * Created by danielmauch on 13.06.16.
 */
public class Player {

    private KnowledgeStones knowledgeStones;
    private String name;
    private Stone[] stones = new Stone[3];
    private PlayerColor color;

    public Player(PlayerColor color, String name) {
        this.name = name;
        this.color = color;
        this.knowledgeStones = new KnowledgeStones();
        for(int i = 0; i < stones.length; i++) {
            stones[i] = new Stone(this.color, i + 1);
        }
    }

    public KnowledgeStones getKnowledgeStones() {
        return knowledgeStones;
    }


    public String getName() {
        return name;
    }

    public Stone[] getStones() {
        return stones;
    }

    public PlayerColor getColor() {
        return color;
    }
}
