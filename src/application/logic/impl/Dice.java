package application.logic.impl;

/**
 * Created by danielmauch on 13.06.16.
 */
public class Dice {

    private int currentRoll;
    private int lastRoll;
    private int tries;

    /**
     * Konstruktor
     */
    public Dice() {
        tries = 0;
        currentRoll = 0;
        lastRoll = 0;
    }

    /**
     * Berechnet Zufallszahl zwischen 1 und 6
     * @return integer zwischen 1 und 6
     */
    public int rollTheDice(){
        lastRoll = currentRoll;
        int roll = (int)(Math.random()*6)+1;
        currentRoll = roll;

        tries++;

        return currentRoll;
    }

    public void reset() {
        lastRoll = 0;
        currentRoll = 0;
        tries = 0;
    }

    public int getTries() {
        return tries;
    }

    public int getLastRoll() {
        return lastRoll;
    }

    public int getCurrentRoll() {
        return currentRoll;
    }
}
