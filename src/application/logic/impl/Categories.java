package application.logic.impl;

/**
 * Created by Daniel on 13.06.2016.
 */
public enum Categories {
    CATEGORY_ONE("Allgemeinbildung"),
    CATEGORY_TWO("Technik"),
    CATEGORY_THREE("Kultur"),
    CATEGORY_FOUR("Wissenschaft");

    private String value;

    Categories(String name) {
        this.value = name;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Kategorie: " + value + '\n';
    }
}
