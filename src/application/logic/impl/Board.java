package application.logic.impl;

/**
 * Implementiert die Logik des Spielbretts und enthaelt Informationen über die Felder des Spielbretts, sowie die
 * Startfelder der einzelnen Spieler.
 *
 * Created by danielmauch on 13.06.16.
 */
public class Board {

    private Stone[] fields = new Stone[48];
    private int[] playerStartFields = new int[4];

    /*
     * Bis zu diesem Feld gilt die Kategorie
     */
    private int categoryOne = 11;
    private int categoryTwo = 23;
    private int categoryThree = 35;
    private int categoryFour = 47;

    /**
     * Konstruktor
     * Initialisiert das Feld mit null. Setzt die Spielerstartfelder.
     */
    public Board(){
        playerStartFields[PlayerColor.RED.getValue()] = 0;
        playerStartFields[PlayerColor.BLUE.getValue()] = 11;
        playerStartFields[PlayerColor.YELLOW.getValue()] = 23;
        playerStartFields[PlayerColor.GREEN.getValue()] = 35;

        for(int i = 0; i < fields.length; i++) {
            fields[i] = null;
        }
    }

    //TODO zu ändern, wenn Fragerunde implementiert wird. -> returnValue = Stone -> Farbinformation für Kollission
    /**
     * Checkt, ob das Feld auf dem Spielbrett bereits belegt ist.
     * @param field Feld auf dem Spielfeld
     * @return Gibt an, ob das gesuchte Feld frei ist.
     */
    public boolean isFieldFree(int field) {

        if(field < 0) {
            field = 48;
        } else {
            field = field % 48;
        }

        if(fields[field] == null) {
            return true;
        }

        return false;
    }

    /**
     * Setz einen Spielstein auf das Spielfeld. Checkt erneut, dass auch wirklich keine Kollission entsteht.
     * @param stone Spielstein
     * @param amountFields Anzahl der Felder, die gezogen werden sollen.
     * @return Gibt am, ob das Setzen erfolgreich war.
     */
    public boolean setStone(Stone stone, int amountFields) {
        if (isFieldFree(amountFields)) {
            fields[amountFields] = stone;
            return true;
        }
        return false;
    }

    /**
     * Entfernt einen Stein vom Spielbrett. Kann durch eine Kollission verursacht werden.
     * @param stone Spielstein
     * @return Gibt an, ob die das Entfernen erfolgreich war.
     */
    public boolean removeStone(Stone stone) {
        if(!isFieldFree(stone.getField())) {
            fields[stone.getField()] = null;
            stone.setField(-1);
            return true;
        }
        return false;
    }

    /**
     * Setzt einen neuen Stein auf das Spielfeld. Kann durch ein Wuerfelergebnis von 6 ausgelöst werden.
     * @param stone
     * @return Gibt an, ob das Setzen erfolgreich war.
     */
    public boolean setNewStone(Stone stone) {
        if(isFieldFree(playerStartFields[stone.getColor().getValue()])) {
            fields[playerStartFields[stone.getColor().getValue()]] = stone;

            return true;
        }
        return false;
    }

    /**
     * Zieht einen Stein um eine gewissen Anzahl von Feldern nach vorne. Wird durch den Wuerfelvorgang ausgelöst.
     * @param stone Spielstein
     * @param amountFields Anzahl der Felder, die gezogen werden sollen.
     * @return
     */
    public boolean moveStone(Stone stone, int amountFields) {
        if(isFieldFree(stone.getField() + amountFields)) {
            fields[(stone.getField() + amountFields) % 48] = stone;
            fields[stone.getField()] = null;

            return true;
        }
        return false;
    }

    public Stone getField(int field) {
        if(field < 0) {
            field = 48;
        } else {
            field = field % 48;
        }

        return fields[field];
    }

    public int[] getPlayerStartFields() {
        return playerStartFields;
    }
}
