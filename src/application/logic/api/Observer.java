package application.logic.api;

/**
 * Created by danielmauch on 13.06.16.
 */
public interface Observer<Info> {

    void update(Info info);
}
