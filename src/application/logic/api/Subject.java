package application.logic.api;

/**
 * Created by danielmauch on 13.06.16.
 */
public interface Subject {

    void detach(Observer obs);
    void attach(Observer obs);
    void notifyAllObservers();
}
