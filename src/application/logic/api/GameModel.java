package application.logic.api;

import application.logic.impl.*;

import java.util.ArrayList;

/**
 * Created by danielmauch on 13.06.16.
 */
public interface GameModel extends Subject{

    void initGame(String name);
    State getState();
    void setState(State state);
    void setNextPlayer(Player player);
    boolean getFieldIsSet(int field);
    boolean addPlayer(String name);
    Players getPlayers();
    int getAmountPlayers();
    void setAmountPlayers(int amount);
    Player getActivePlayer();
    ArrayList<Player> getPlayersInOrder();
    Dice getDice();
    boolean isInit();
    OUTPUT getOutput();
    int getCurrentStone();

    /**
     * Systemoperationen
     */
    void startMove();
    void rollTheDice();
    void selectStone(int stoneNr);
    void setNewStone();
    void endRound();
}
