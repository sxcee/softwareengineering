package application.logic.api.impl;

import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.api.Observer;
import application.logic.impl.*;
import application.logic.impl.OUTPUT;

import java.util.ArrayList;

/**
 * Created by danielmauch on 13.06.16.
 */
public class GameModelImpl implements GameModel {

    private Players players;
    private ArrayList<Player> playersInOrder;
    private Player activePlayer;
    private int currentPlayer;
    private int currentStone;
    private int amountPlayers;
    private Dice dice;
    private State gameState;
    private Board board;
    private boolean init;
    private static int boardSize = 48;

    private OUTPUT output;

    private ArrayList<Observer> observers = new ArrayList<Observer>();

    public GameModelImpl() {
        this.dice = new Dice();
        this.gameState = State.SETUP;
        this.board = new Board();
        this.players = new Players();
        this.amountPlayers = 0;
        this.playersInOrder = new ArrayList<>();
        this.init = true;
        this.output = OUTPUT.NO_MESSAGE;
        this.currentPlayer = 1;
        this.currentStone = -1;
    }

    @Override
    public void detach(Observer obs) {
        //Views und Controller müssen sich abmelden, wenn sie an der Reihen waren.
        observers.remove(obs);
    }

    @Override
    public void attach(Observer obs) {
        //Wenn der Zustand gewechselt wird müssen sich die Views und Controller hier anmelden.
        observers.add(obs);
    }

    @Override
    public int getAmountPlayers() {
        return amountPlayers;
    }

    @Override
    public void notifyAllObservers() {
        //Hier sollten nur noch die richtigen Views und Controller angesprochen werden.
        for (Observer obs : observers) {
            obs.update(this);
        }
    }

    @Override
    public Players getPlayers() {
        return players;
    }

    @Override
    public void initGame(String name) {
        //Players missing - mit namen Player erzeugen und Players hinzufügen
        //notify()
    }

    @Override
    public void startMove() {
        init = false;
        gameState = State.DARFWUERFELN;
        notifyAllObservers();
    }

    @Override
    public void rollTheDice() {
        int result = this.dice.rollTheDice();

        gameState = State.WUERFELERGEBNIS_EVAL;
        resultEval();
        notifyAllObservers();
    }

    public void resultEval() {
        boolean threeTimes = true;
        boolean someoneAtHome = false;
        Player player = APIFactory.factory.getModel().getActivePlayer();
        for(Stone stone : player.getStones()) {
            if(stone.getField() != -1) {
                threeTimes = false;
            }
            if(stone.getField() == -1) {
                someoneAtHome = true;
            }
        }

        if(threeTimes && dice.getTries() <= 3) {
            if(dice.getCurrentRoll() == 6) {
                gameState = State.DARFRAUS;
            } else {
                gameState = State.DARFWUERFELN;
            }
        }

        if(dice.getCurrentRoll() != 6 && dice.getTries() == 3) {
            gameState = State.ENDE;
            output = OUTPUT.NO_SIX;
        }

        if(!threeTimes){
            if(dice.getCurrentRoll() == 6) {
                if(!someoneAtHome) {
                    gameState = State.DARFZIEHEN;
                } else {
                    if(board.isFieldFree(board.getPlayerStartFields()[activePlayer.getColor().getValue()])) {
                        gameState = State.DARFRAUS;
                    } else {
                        gameState = State.ENDE;
                        output = OUTPUT.BASE_OCCUPIED;
                    }
                }
            } else {
                gameState = State.DARFZIEHEN;
            }
        }
    }

    public boolean isInit() {
        return init;
    }

    @Override
    public void endRound() {
        dice.reset();
        activePlayer = getNextPlayer();

        gameState = State.INIT;
        output = OUTPUT.NO_MESSAGE;

        notifyAllObservers();
    }

    public Player getNextPlayer() {
        if (currentPlayer + 1 > amountPlayers) {
            currentPlayer = 1;
        } else {
            currentPlayer++;
        }

        return playersInOrder.get(currentPlayer - 1);
    }

    @Override
    public void selectStone(int stoneNr) {
        boolean stoneAvailable = false;
        for (Stone stone : activePlayer.getStones()) {
            if(stone.getStoneNr() == stoneNr && stone.getField() != -1) {
                stoneAvailable = true;
            }
        }
        if (stoneAvailable && stoneNr <= 3 && stoneNr >= 1) {
            currentStone = stoneNr - 1;

            if (board.isFieldFree((activePlayer.getStones()[stoneNr - 1].getField() + dice.getCurrentRoll()) % boardSize)) {
                board.moveStone(activePlayer.getStones()[stoneNr - 1], dice.getCurrentRoll());
                activePlayer.getStones()[stoneNr - 1].setField(activePlayer.getStones()[stoneNr - 1].getField() + dice.getCurrentRoll() % boardSize);
                gameState = State.ENDE;
                output = OUTPUT.SUCCES_MOVESTONE;
            } else {
                gameState = State.ENDE;
                Stone stone = board.getField(activePlayer.getStones()[stoneNr - 1].getField() + dice.getCurrentRoll());

                if(stone != null) {
                    if(stone.getColor().equals(activePlayer.getColor())) {
                        output = OUTPUT.FIELD_OCCUPIED_OWN;
                        gameState = State.DARFZIEHEN;
                    } else {
                        output = OUTPUT.FIELD_OCCUPIED;
                        gameState = State.ENDE;
                    }
                }
            }
        } else if (!stoneAvailable) {
            //do nothing
        } else {
            gameState = State.DARFZIEHEN;
        }
        notifyAllObservers();
    }

    @Override
    public void setNewStone() {
        for (Stone stone : activePlayer.getStones()) {
            if (stone.getField() == -1) {
                boolean success = board.setNewStone(activePlayer.getStones()[stone.getStoneNr() - 1]);
                if(success) {
                    stone.setField(board.getPlayerStartFields()[stone.getColor().getValue()]);
                    gameState = State.ENDE;
                    output = OUTPUT.SUCCESS_NEWSTONE;
                } else {
                    gameState = State.ENDE;
                    output = OUTPUT.BASE_OCCUPIED;
                }
                break;
            }
        }
        notifyAllObservers();
    }

    @Override
    public State getState() {
        return this.gameState;
    }

    @Override
    public void setState(State state) {

    }

    @Override
    public void setNextPlayer(Player player) {

    }

    @Override
    public void setAmountPlayers(int amountPlayers) {
        if(amountPlayers < 2 || amountPlayers > 4) {
            amountPlayers = 0;
        }

        this.amountPlayers = amountPlayers;
        notifyAllObservers();
    }

    @Override
    public boolean getFieldIsSet(int field) {
        return false;
    }

    @Override
    public boolean addPlayer(String name) {
        boolean isDuplicate = false;

        if (name != null && !name.equals("")) {
            int color = this.getPlayers().getPlayers().size();

            for (Player player : players.getPlayers()) {
                if (player.getName().equalsIgnoreCase(name)) {
                    isDuplicate = true;
                }
            }

            if (!isDuplicate) {
                Player player = new Player(PlayerColor.values()[color], name);
                players.addPlayer(player);
            }

            if(!isDuplicate && players.getPlayers().size() == amountPlayers) {
                gameState = State.INIT;
                calcPlayerOrder();
                activePlayer = playersInOrder.get(0);
            }

            if(isDuplicate) {
                gameState = State.SETUP;
            }

            notifyAllObservers();

            return true;
        }

        notifyAllObservers();
        return false;
    }

    public void calcPlayerOrder(){
        int[] playerID = new int[players.getPlayers().size()];
        int index;

        for (int i = 0; i < players.getPlayers().size(); i++) {
            playerID[i] = (int) (Math.random()*100)+1;
        }

        for (int i = 0; i < players.getPlayers().size(); i++) {
            index = getMaxIndex(playerID);
            playerID[index] = -1;
            playersInOrder.add(i, players.getPlayers().get(index));
        }
    }



    private int getMaxIndex(int[] list) {
        int max = -1;
        int index = 5;
        for (int i = 0; i < list.length; i++) {
            if(list[i] > max) {
                max = list[i];
                index = i;
            }
        }
        return index;
    }

    public Dice getDice() {
        return dice;
    }

    @Override
    public Player getActivePlayer() {
        return activePlayer;
    }

    @Override
    public ArrayList<Player> getPlayersInOrder() {
        return playersInOrder;
    }

    @Override
    public OUTPUT getOutput() {
        return output;
    }

    @Override
    public int getCurrentStone() {
        return currentStone;
    }
}
