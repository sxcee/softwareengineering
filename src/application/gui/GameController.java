package application.gui;

import application.logic.APIFactory;
import application.logic.api.Observer;
import application.logic.impl.State;

/**
 * Created by danielmauch on 13.06.16.
 */
public abstract class GameController implements Observer {

    public State controllerState;
}
