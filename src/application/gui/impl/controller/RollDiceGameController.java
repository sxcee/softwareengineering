package application.gui.impl.controller;

import application.gui.GameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.impl.State;

import java.util.Scanner;

/**
 * Created by danielmauch on 13.06.16.
 */
public class RollDiceGameController extends GameController{

    public RollDiceGameController() {
        controllerState = State.DARFWUERFELN;
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(controllerState)){
            waitForConfirm();
        }
    }
    
    public void waitForConfirm() {
        String input = null;
        Scanner scanner = new Scanner(System.in);
        input = scanner.nextLine();

        APIFactory.factory.getModel().rollTheDice();
    }
}
