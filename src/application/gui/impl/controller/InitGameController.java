package application.gui.impl.controller;

import application.gui.GameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.impl.State;

import java.util.Scanner;

/**
 * Created by danielmauch on 13.06.16.
 */
public class InitGameController extends GameController {

    public InitGameController(){
        controllerState = State.INIT;
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(controllerState)){
            waitForConfirm();
        }
    }

    public void waitForConfirm() {
        String input = null;
        Scanner inputReader = new Scanner(System.in);
        input = inputReader.nextLine();

        APIFactory.factory.getModel().startMove();
    }
}
