package application.gui.impl.controller;

import application.gui.GameController;
import application.logic.APIFactory;
import application.logic.api.Observer;
import application.logic.impl.State;

import java.util.Scanner;

/**
 * Created by danielmauch on 14.06.16.
 */
public class EndGameController extends GameController {



    public EndGameController() {
        controllerState = State.ENDE;
    }

    @Override
    public void update(Object o) {
        if (controllerState.equals(State.ENDE)) {
            waitForConfirm();
        }
    }

    public void waitForConfirm() {
//        Scanner scanner = new Scanner(System.in);
//        String input = null;
//        input = scanner.nextLine();

        APIFactory.factory.getModel().endRound();
    }
}
