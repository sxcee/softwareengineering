package application.gui.impl.controller;

import application.gui.GameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.impl.State;

import java.util.Scanner;

/**
 * Created by danielmauch on 13.06.16.
 */
public class NewStoneGameController extends GameController {

    public NewStoneGameController() {
        controllerState = State.DARFRAUS;
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(controllerState)){
            APIFactory.factory.getModel().setNewStone();
        }
    }
}
