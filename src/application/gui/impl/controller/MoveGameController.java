package application.gui.impl.controller;

import application.gui.GameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.impl.State;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by danielmauch on 13.06.16.
 */
public class MoveGameController extends GameController {

    public MoveGameController() {
        controllerState = State.DARFZIEHEN;
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(controllerState)){
            waitForChoice();
        }
    }

    public void waitForChoice() {
        String input = null;
        Scanner inputReader = new Scanner(System.in);
        input = inputReader.nextLine();
        int choice = 0;

        if((input != null && input.equals("")) || Pattern.matches("[1-3]", input) == false) {
            APIFactory.factory.getModel().selectStone(0);
        } else {
            choice = Integer.parseInt(input);
            APIFactory.factory.getModel().selectStone(choice);
        }
    }
}
