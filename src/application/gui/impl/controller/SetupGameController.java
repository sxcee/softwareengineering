package application.gui.impl.controller;

import application.gui.GameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.impl.State;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by Daniel on 13.06.2016.
 */
public class SetupGameController extends GameController {

    public SetupGameController() {
        controllerState = State.SETUP;
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(controllerState)){
            waitForInput();
        }
    }

    public void waitForInput() {
        if (APIFactory.factory.getModel().getAmountPlayers() == 0) {
            waitForAmountOfPlayers();
        } else {
            if (APIFactory.factory.getModel().getPlayers().getPlayers().size() < APIFactory.factory.getModel().getAmountPlayers()) {
                waitForPlayerName();
            }
        }
    }

    public void waitForAmountOfPlayers() {
        String input = null;
        Scanner inputReader = new Scanner(System.in);
        input = inputReader.nextLine();

        if ((input != null && input.equals("")) || Pattern.matches("[2-4]", input) == false) {
            APIFactory.factory.getModel().setAmountPlayers(0);
        } else {
            int amount = Integer.parseInt(input);
            APIFactory.factory.getModel().setAmountPlayers(amount);
        }
    }

    public void waitForPlayerName() {
        String input = null;
        Scanner inputReader = new Scanner(System.in);
        input = inputReader.nextLine();

        APIFactory.factory.getModel().addPlayer(input);
    }
}
