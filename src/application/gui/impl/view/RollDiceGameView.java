package application.gui.impl.view;

import application.gui.GameView;
import application.gui.ViewConstants;
import application.gui.impl.controller.RollDiceGameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.api.Observer;
import application.logic.impl.Player;
import application.logic.impl.State;
import application.logic.impl.Stone;

/**
 * Created by danielmauch on 13.06.16.
 */
public class RollDiceGameView implements Observer {

    private State viewState;
    private RollDiceGameController controller;
    private ViewConstants constants;

    public RollDiceGameView() {
        controller = new RollDiceGameController();
        viewState = State.DARFWUERFELN;
        constants = new ViewConstants();
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(viewState)){
            displayRollDice();
        }
    }

    public void displayRollDice() {
        boolean threeTimes = true;
        Player player = APIFactory.factory.getModel().getActivePlayer();
        for(Stone stone : player.getStones()) {
            if(stone.getField() != -1) {
                threeTimes = false;
            }
        }

        if (threeTimes) {
            if(APIFactory.factory.getModel().getDice().getTries() == 0) {
                GameView.clearConsole();
                System.out.println(constants.rollDiceThreeTries);
                System.out.println(constants.rollDiceFirstOutput);
            } else {
                System.out.println(constants.rollDiceResult + APIFactory.factory.getModel().getDice().getCurrentRoll());
            }
        } else {
            if(APIFactory.factory.getModel().getDice().getTries() == 0) {
                GameView.clearConsole();
                System.out.println(constants.rollDiceFirstOutput);
            } else {
                System.out.println(constants.rollDiceResult + APIFactory.factory.getModel().getDice().getCurrentRoll());
            }
        }
    }

    public RollDiceGameController getController() {
        return controller;
    }
}
