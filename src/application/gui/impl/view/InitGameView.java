package application.gui.impl.view;

import application.gui.GameView;
import application.gui.ViewConstants;
import application.gui.impl.controller.InitGameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.api.Observer;
import application.logic.impl.Player;
import application.logic.impl.State;

import java.util.ArrayList;

/**
 * Created by danielmauch on 13.06.16.
 */
public class InitGameView implements Observer {

    private ViewConstants constants;
    private State viewState;
    private InitGameController controller;

    public InitGameView() {
        viewState = State.INIT;
        constants = new ViewConstants();
        controller = new InitGameController();
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(viewState)){
            displayInitScreen();
            displayProgress();
        }
    }

    public void displayInitScreen() {
        //GameView.clearConsole();
        if(APIFactory.factory.getModel().isInit()) {
            System.out.println();
            System.out.println(constants.initFirstOutput);

            ArrayList<Player> listPlayers = APIFactory.factory.getModel().getPlayersInOrder();
            int i = 1;
            for (Player player : listPlayers) {
                System.out.print(i);
                System.out.print(constants.initPlayerList);
                System.out.println(player.getName());
                i++;
            }
        }

        System.out.println(APIFactory.factory.getModel().getActivePlayer().getName() + constants.initFirstPlayer);
        System.out.println(constants.initConfirm);
    }

    public void displayProgress() {

    }

    public InitGameController getController() {
        return controller;
    }
}
