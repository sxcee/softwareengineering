package application.gui.impl.view;

import application.gui.GameView;
import application.gui.ViewConstants;
import application.gui.impl.controller.NewStoneGameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.api.Observer;
import application.logic.impl.State;

/**
 * Created by danielmauch on 13.06.16.
 */
public class NewStoneGameView implements Observer {

    private State viewState;
    private NewStoneGameController controller;
    private ViewConstants constants;

    public NewStoneGameView() {
        controller = new NewStoneGameController();
        viewState = State.DARFRAUS;
        constants = new ViewConstants();
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(viewState)){
            displayNewStone();
        }
    }

    public void displayNewStone() {
        int diceResult = APIFactory.factory.getModel().getDice().getCurrentRoll();

        //GameView.clearConsole();
        System.out.println(constants.newStoneDiceResult + diceResult);
        System.out.println(constants.newStoneSuccess);
        System.out.println();
    }

    public NewStoneGameController getController() {
        return controller;
    }
}
