package application.gui.impl.view;

import application.gui.ViewConstants;
import application.gui.impl.controller.EndGameController;
import application.gui.impl.controller.NewStoneGameController;
import application.logic.APIFactory;
import application.logic.api.Observer;
import application.logic.impl.OUTPUT;
import application.logic.impl.State;
import application.logic.impl.Stone;

/**
 * Created by danielmauch on 14.06.16.
 */
public class EndGameView implements Observer {

    private State viewState;
    private EndGameController controller;
    private ViewConstants constants;

    public EndGameView() {
        controller = new EndGameController();
        viewState = State.ENDE;
        constants = new ViewConstants();
    }

    @Override
    public void update(Object o) {
        if(viewState.equals(State.ENDE)) {
            displayEndView();
        }
    }

    public void displayEndView(){
        OUTPUT output = APIFactory.factory.getModel().getOutput();
        int diceResult = APIFactory.factory.getModel().getDice().getCurrentRoll();

        if(output.equals(OUTPUT.BASE_OCCUPIED)) {
            System.out.println(constants.endDiceResult + diceResult);
            System.out.println(constants.endViewBaseOccupied);
            System.out.println();
        }

        if(output.equals(OUTPUT.FIELD_OCCUPIED)) {
            System.out.println(constants.endViewFieldOccupied);
            System.out.println();
        }

        if(output.equals(OUTPUT.NO_SIX)) {
            System.out.println(constants.endDiceResult + diceResult);
            System.out.println(constants.endViewNoSix);
            System.out.println();
        }

        if(output.equals(OUTPUT.SUCCES_MOVESTONE)) {
            int currentStone = APIFactory.factory.getModel().getCurrentStone();
            Stone stone = APIFactory.factory.getModel().getActivePlayer().getStones()[currentStone];
            System.out.println("Dein Stein Nr " + currentStone + 1 +
            " wurde erfolgreich auf Feld " + stone.getField()  + " verschoben.");
            System.out.println();
        }

//        if(output.equals(OUTPUT.SUCCESS_NEWSTONE)) {
//            System.out.println(constants.endViewSuccessNewStone);
//        }
    }

    public EndGameController getController() {
        return controller;
    }
}
