package application.gui.impl.view;

import application.gui.GameView;
import application.gui.ViewConstants;
import application.gui.impl.controller.SetupGameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.api.Observer;
import application.logic.impl.PlayerColor;
import application.logic.impl.State;

/**
 * Created by Daniel on 13.06.2016.
 */
public class SetupGameView implements Observer {

    private State viewState;
    private ViewConstants constants;
    private SetupGameController controller;

    public SetupGameView() {
        controller = new SetupGameController();
        viewState = State.SETUP;
        constants = new ViewConstants();
    }

    public SetupGameController getController() {
        return controller;
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(viewState)){
            displayOneToFourPlayers();
        }
    }

    public void displayOneToFourPlayers(){
        if(APIFactory.factory.getModel().getPlayers().getPlayers().isEmpty() && APIFactory.factory.getModel().getAmountPlayers() == 0) {
            GameView.clearConsole();
            System.out.print(ViewConstants.setupFirstOutput);
        } else if (APIFactory.factory.getModel().getPlayers().getPlayers().isEmpty() && APIFactory.factory.getModel().getAmountPlayers() != 0) {
            System.out.println(constants.setupHowTo);
            System.out.print(PlayerColor.values()[APIFactory.factory.getModel().getPlayers().getPlayers().size()].toString() +
                    constants.setupPlayerName);
        } else {
            System.out.print(PlayerColor.values()[APIFactory.factory.getModel().getPlayers().getPlayers().size()].toString() + constants.setupPlayerName);
        }
    }
}
