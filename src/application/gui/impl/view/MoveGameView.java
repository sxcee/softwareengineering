package application.gui.impl.view;

import application.gui.GameView;
import application.gui.ViewConstants;
import application.gui.impl.controller.MoveGameController;
import application.logic.APIFactory;
import application.logic.api.GameModel;
import application.logic.api.Observer;
import application.logic.impl.Player;
import application.logic.impl.State;
import application.logic.impl.Stone;

/**
 * Created by danielmauch on 13.06.16.
 */
public class MoveGameView implements Observer {

    private State viewState;
    private MoveGameController controller;
    private ViewConstants constants;

    public MoveGameView() {
        controller = new MoveGameController();
        viewState = State.DARFZIEHEN;
        constants = new ViewConstants();
    }

    @Override
    public void update(Object o) {
        GameModel model = (GameModel) o;

        if(model.getState().equals(viewState)){
            displayMove();
        }
    }

    public void displayMove() {
        Player activePlayer = APIFactory.factory.getModel().getActivePlayer();
        int diceResult = APIFactory.factory.getModel().getDice().getCurrentRoll();
        GameView.clearConsole();
        System.out.println(constants.moveDiceResult + diceResult);
        System.out.println(constants.moveFirstOutput);

        for(Stone stone : activePlayer.getStones()) {
            if (stone.getField() != -1) {
                System.out.println("Stein " + stone.getStoneNr() + " auf Feld " + stone.getField() + ".");
            }
        }

        System.out.print(constants.moveChoose);
    }

    public MoveGameController getController() {
        return controller;
    }
}
