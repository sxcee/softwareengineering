package application.gui;

import application.gui.impl.controller.RollDiceGameController;
import application.gui.impl.view.*;
import application.logic.APIFactory;
import application.logic.api.Observer;
import application.logic.impl.State;

import java.util.Set;

/**
 * Created by danielmauch on 13.06.16.
 */
public class GameView {

    public GameView() {
        attachObserver();
    }

    public final static void clearConsole() {
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

    public void attachObserver() {
        SetupGameView setupView = new SetupGameView();
        MoveGameView moveView = new MoveGameView();
        NewStoneGameView newStoneView = new NewStoneGameView();
        RollDiceGameView rollDiceView = new RollDiceGameView();
        InitGameView initView = new InitGameView();
        EndGameView endView = new EndGameView();

        APIFactory.factory.getModel().attach(endView);
        APIFactory.factory.getModel().attach(setupView);
        APIFactory.factory.getModel().attach(moveView);
        APIFactory.factory.getModel().attach(newStoneView);
        APIFactory.factory.getModel().attach(rollDiceView);
        APIFactory.factory.getModel().attach(initView);

        APIFactory.factory.getModel().attach(setupView.getController());
        APIFactory.factory.getModel().attach(moveView.getController());
        APIFactory.factory.getModel().attach(newStoneView.getController());
        APIFactory.factory.getModel().attach(rollDiceView.getController());
        APIFactory.factory.getModel().attach(initView.getController());
        APIFactory.factory.getModel().attach(endView.getController());
    }

    public void startEventLoop() {
        APIFactory.factory.getModel().notifyAllObservers();
    }
}
