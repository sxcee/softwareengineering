package application.gui;

/**
 * Created by danielmauch on 14.06.16.
 */
public class ViewConstants {

    /*
        Konstanten für den Setup View
     */
    public static String setupFirstOutput = "Willkommen Spielefreunde !" + "\n" +
            "Bitte gebe die Menge der Spieler ein. -> ";

    public static String setupPlayerName = "Spieler: ";

    public static String setupHowTo = "Bitte gebe die Namen in der Konsole ein und bestätige mit <Enter>.";

    /*
        Konstanten für den Init View
     */
    public static String initLastPlayerEnd = " deine Runde ist beendet.";

    public static String initFirstOutput = "Es wurden folgende Reihefolge ausgewählt:";

    public static String initPlayerList = ". Spieler: ";

    public static String initFirstPlayer = ", du bist der nächste Spieler!";

    public static String initStoneOnField = "Du hast folgende Steine auf dem Spielfeld:";

    public static String initConfirm = "Bitte bestätige den Anfang deines Zuges durch <Enter>";

    /*
        Konstanten für den RollDice View
     */
    public static String rollDiceThreeTries = "Da du keine Steine auf dem Feld hast, darfst du bis zu drei mal würfeln!";

    public static String rollDiceFirstOutput = "Bitte würfle mit <Enter>";

    public static String rollDiceResult = "Deine gewürfelte Augenzahl ist: ";

    /*
        Konstanten für den NewStone View
     */
    public static String newStoneDiceResult = "Deine gewürfelte Augenzahl ist: ";

    public static String newStoneSuccess = "Du hast eine Sechs gewürfelt und ein neuer Stein wurde auf deine Base gesetzt";

    /*
        Konstanten für den Move View
     */
    public static String moveDiceResult = "Deine gewürfelte Augenzahl ist: ";

    public static String moveFirstOutput = "Du hast die folgenden Steine auf dem Feld: ";

    public static String moveChoose = "Bitte suche dir den Stein aus, mit dem du fahren möchtest.\n" +
            "Gebe die Nummer des Steins an und bestätige mit <Enter>: ";

    /*
        Konstanten für den End View
     */
    public static String endDiceResult = "Deine gewürfelte Augenzahl ist: ";

    public static String endViewBaseOccupied = "Deine Base ist leider belegt. Es kann kein neuer Stein aufs Spielfeld gesetzt werden.";

    public static String endViewNoSix = "Leider hast du keine Sechs gewürfelt.";

    public static String endViewFieldOccupied = "Das Feld ist leider schon belegt. -> Hier würde die Fragerunde beginnen.";

    public static String endViewSuccessNewStone = "Es wurde erfolgreich ein neuer Stein auf deine Base gestellt.";

    public static String endViewSuccessMoveStone = "Dein Stein Nr wurde erfolgreich verschoben.";

    public static String endViewConfirm = "Bestätige mit <Enter> das Ende deines Spielzuges.";
}
